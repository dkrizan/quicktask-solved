<?php
namespace App\Presenters;

/**
 * Class TaskPresenter
 * @package App\Presenters
 */
class TaskPresenter extends BasePresenter
{
    /** @var \App\Model\Repository\TaskGroupRepository @inject */
    public $taskGroupRepository;
    /** @var \App\Model\Repository\TaskRepository @inject */
    public $taskRepository;
    /** @var \App\Model\Repository\CategoryRepository @inject */
    public $categoryRepository;
    /** @var \App\Factories\Modal\IInsertTaskGroupFactory @inject */
    public $insertTaskGroupFactory;
    /** @var \App\Factories\Form\IInsertTaskFactory @inject */
    public $insertTaskFactory;
    /** @var number */
    protected $idTaskGroup;

    public function renderDefault()
    {
        $this->template->taskGroups = $this->getTaskGroups();
    }

    /**
     * @param string $snippet
     */
    public function ajaxRedrawTasks() {
        if ($this->isAjax()) {
            $this->redrawControl('tasks');
        } else {
            $this->redirect('this');
        }
    }
    /**
     * @param int $id
     */
    public function handleDeleteTaskGroup($id)
    {
        $this->taskGroupRepository->delete($id);
        $this->ajaxRedrawTasks();
    }

    /**
     * @param int $id
     */
    public function handleDeleteTask($id)
    {
        $this->taskRepository->delete($id);
        $this->ajaxRedrawTasks();
    }

    /**
     * @param int $id
     */
    public function handleCheckTask($idTask)
    {
        $this->taskRepository->updateTaskComplete($idTask);
        $this->ajaxRedrawTasks();
    }

    /**
     * @param int $id
     */
    public function handleUnCheckTask($idTask)
    {
        $this->taskRepository->updateTaskUnComplete($idTask);
        $this->ajaxRedrawTasks();
    }
    /**
     * @param number $idTaskGroup
     */

    /**
     * @param int $id
     */
    public function handleFilterTasks($idTaskGroup,$name)
    {
        $this->template->tasks = $this->taskRepository->getByTaskGroupAndName($idTaskGroup,$name);

    }
    /**
     * @param number $idTaskGroup
     */

    public function renderTaskGroup($idTaskGroup)
    {
        $this->idTaskGroup = $idTaskGroup;
        $this->template->idTaskGroup = $idTaskGroup;
        $this->template->tasks = $this->getTasks($idTaskGroup);
    }

    /**
     * @return \App\Components\Modal\InsertTaskGroup
     */
    protected function createComponentInsertTaskGroupModal()
    {
        $control = $this->insertTaskGroupFactory->create();
        return $control;
    }

    /**
     * @return \App\Components\Form\InsertTask
     */
    protected function createComponentInsertTaskForm()
    {
        $control = $this->insertTaskFactory->create();
        $control->setTaskGroupId($this->idTaskGroup);
        return $control;
    }

    /**
     * @return array
     */
    protected function getTaskGroups()
    {
        $result = array();
        $taskGroups = $this->taskGroupRepository->getAll();
        foreach ($taskGroups as $taskGroup) {
            $item = array();
            $item['id'] = $taskGroup->getId();
            $item['name'] = $taskGroup->getName();
            $result[] = $item;
        }
        return $result;
    }

    /**
     * @param number $idTaskGroup
     * @return array
     */
    protected function getTasks($idTaskGroup)
    {
        $result = array();
        $tasks = $this->taskRepository->getByTaskGroup($idTaskGroup);
        foreach ($tasks as $task) {
            $item = array();
            $item['id'] = $task->getId();
            $item['date'] = $task->getDate();
            $item['name'] = $task->getName();
            $item['completed'] = $task->getCompleted();
            $item['category'] = $task->getCategory() ? $task->getCategory()->getName() : null;
            $result[] = $item;
        }
        return $result;
    }

    /**
     * @param number $idTaskGroup
     * @return array
     */
    protected function getTasksByName($idTaskGroup,$name)
    {
        $result = array();
        $tasks = $this->taskRepository->getByTaskGroupAndName($idTaskGroup,$name);
        foreach ($tasks as $task) {
            $item = array();
            $item['id'] = $task->getId();
            $item['date'] = $task->getDate();
            $item['name'] = $task->getName();
            $item['completed'] = $task->getCompleted();
            $result[] = $item;
        }
        return $result;
    }
}
