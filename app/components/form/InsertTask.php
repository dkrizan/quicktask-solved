<?php
namespace App\Components\Form;

use App\Model\Entity\Task;
use App\Model\Entity\TaskGroup;
use App\Model\Repository\CategoryRepository;
use App\Model\Repository\TaskRepository;
use App\Model\Repository\TaskGroupRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class InsertTask extends Control
{
    /** @var TaskRepository*/
    public $taskRepository;
    /** @var TaskGroupRepository*/
    public $taskGroupRepository;
    /** @var CategoryRepository*/
    public $categoryRepository;
    /** @var number */
    public $idTaskGroup;



    /**
     * @param TaskRepository $taskRepository
     * @param TaskGroupRepository $taskGroupRepository
     */
    public function __construct(TaskRepository $taskRepository, TaskGroupRepository $taskGroupRepository, CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->taskRepository = $taskRepository;
        $this->taskGroupRepository = $taskGroupRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/InsertTask.latte');
        $template->render();
    }

    /**
     * @param int $idTaskGroup
     */
    public function setTaskGroupId($idTaskGroup)
    {
        $this->idTaskGroup = $idTaskGroup;
    }

    /**
     * @return Form
     */
    protected function createComponentInsertTaskForm()
    {
        $form = new Form();
        $form->addText('name', 'Name')
            ->setRequired('Please fill task name');
        $form->addText('date', 'Date')
            ->setRequired('Please fill task date');
        $form->addSelect('category','Category',$this->getCategories());
        $form->addHidden('idTaskGroup', $this->idTaskGroup);
        $form->addSubmit('submit', 'Add');
        $form->onSuccess[] = array($this, 'insertTaskFromSuccess');
        return $form;
    }

    /**
     * @param Form $form
     * @param $values
     */
    public function insertTaskFromSuccess(Form $form, $values)
    {
        $taskGroup = $this->taskGroupRepository->getById($values->idTaskGroup);
        $category = $this->categoryRepository->getById($values->category);
        $taskEntity = new Task();
        $taskEntity->setName($values->name);
        $taskEntity->setDate($values->date);
        $taskEntity->setTaskGroup($taskGroup);
        $taskEntity->setCategory($category);
        $this->taskRepository->insert($taskEntity);
        $this->presenter->flashMessage('Task was created', 'success');
        $this->template->tasks = $this->getTasks($values->idTaskGroup);
        if($this->presenter->isAjax()) {
            $this->presenter->redrawControl("tasks");
        } else {
            $this->redirect('this');
        }
    }

    protected function getTasks($idTaskGroup)
    {
        $result = array();
        $tasks = $this->taskRepository->getByTaskGroup($idTaskGroup);
        foreach ($tasks as $task) {
            $item = array();
            $item['id'] = $task->getId();
            $item['date'] = $task->getDate();
            $item['name'] = $task->getName();
            $item['completed'] = $task->getCompleted();
            $item['category'] = $task->getCategory()->getName();
            $result[] = $item;
        }
        return $result;
    }

    protected function getCategories()
    {
        $result = array();
        $categories = $this->categoryRepository->getAll();
        foreach ($categories as $category) {
            $result[$category->getId()] = $category->getName();
        }
        return $result;
    }
}
