<?php
namespace App\Model\Repository;

use App\Model\Entity;
use Kdyby\Doctrine\EntityManager;

class TaskRepository extends AbstractRepository
{
    /** @var \Kdyby\Doctrine\EntityRepository */
    private $task;

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->task = $this->entityManager->getRepository(Entity\Task::getClassName());
    }

    /**
     * @param number $id
     * @return Entity\Task|null
     */
    public function getById($id)
    {
        return $this->task->find($id);
    }

    /**
     * @param number $idTaskGroup
     * @return Entity\Task[]
     */
    public function getByTaskGroup($idTaskGroup)
    {
        return $this->task->findBy(array('taskGroup' => $idTaskGroup),array('date' => 'DESC'));
    }

    /**
     * @param number $idCategory
     * @return Entity\Task[]
     */
    public function getByCategory($idCategory)
    {
        return $this->task->findBy(array('taskGroup' => $idCategory));
    }

    /**
     * @param number $idTaskGroup
     * @param string $name
     * @return Entity\Task[]
     */
    public function getByTaskGroupAndName($idTaskGroup,$name)
    {
        return $this->task->findBy(array('taskGroup' => $idTaskGroup, 'name' => $name),array('date' => 'DESC'));
    }

    /**
     * @param Entity\Task $task
     */
    public function insert(Entity\Task $task)
    {
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * @param number $id
     */
    public function updateTaskComplete($id)
    {
        $task = $this->task->find($id);
        $task->setCompleted(TRUE);
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * @param number $id
     */
    public function updateTaskUnComplete($id)
    {
        $task = $this->task->find($id);
        $task->setCompleted(FALSE);
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

}
