<?php
namespace App\Model\Repository;

use App\Model\Entity;
use Kdyby\Doctrine\EntityManager;

class CategoryRepository extends AbstractRepository
{
    /** @var \Kdyby\Doctrine\EntityRepository */
    private $category;

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->category = $this->entityManager->getRepository(Entity\Category::getClassName());
    }

    /**
     * @param number $id
     * @return Entity\Category|null
     */
    public function getById($id)
    {
        return $this->category->find($id);
    }

    /**
     * @return Entity\Category[]
     */
    public function getAll()
    {
        return $this->category->findBy(array());
    }
    
    /**
     * @param Entity\Category $category
     */
    public function insert(Entity\Category $category)
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }


}
