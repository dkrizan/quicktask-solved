$(document).ready(function(){
    $.nette.init();

    $('.datepicker').datepicker({
        orientation: 'left bottom'
    });
    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // optional
    });
});